module "aurora" {
  source = "./modules/aws-aurora-serverless"

  depends_on = [module.vpc]

  application                      = var.application
  database_name                    = "technicalTest"
  db_subnets                       = module.vpc.private_subnet_ids
  engine                           = "aurora-postgresql"
  engine_mode                      = "serverless"
  engine_version                   = "10.7"
  environment                      = var.environment
  port                             = 5432
  project                          = var.project
  scaling_auto_pause               = true
  scaling_max_capacity             = 8
  scaling_min_capacity             = 2
  scaling_seconds_until_auto_pause = 300
  scaling_timeout_action           = "ForceApplyCapacityChange"
  vpc_cidr                         = var.vpc_cidr
  vpc_id                           = module.vpc.vpc_id
}

resource "aws_security_group" "qledger" {

  name_prefix = "qledger-"
  vpc_id      = module.vpc.vpc_id

  description = "Attached to QLedger"

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-qledger"
    )
  )
}

resource "aws_security_group_rule" "allow_access" {
  type                     = "ingress"
  from_port                = module.aurora.aurora_port
  to_port                  = module.aurora.aurora_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.qledger.id
  security_group_id        = module.aurora.aurora_security_group_id
}
