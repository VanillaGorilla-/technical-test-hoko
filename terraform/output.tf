output "vpc_info" {
  value = [
    module.vpc.vpc_id,
    module.vpc.public_subnet_cidrs,
    module.vpc.public_subnet_ids,
    module.vpc.private_subnet_cidrs,
    module.vpc.private_subnet_ids,
    module.vpc.flow_log_role_name,
    module.vpc.flow_log_policy_arn
  ]
}
