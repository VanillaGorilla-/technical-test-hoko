output "vpc_id" {
  description = "Logical AWS ID of the VPC."
  value       = aws_vpc.main.id
}

output "private_subnet_cidrs" {
  description = "List of CIDR blocks of private subnets"
  value       = aws_subnet.private_subnets.*.cidr_block
}

output "private_subnet_ids" {
  description = "List of IDs of private subnets"
  value       = aws_subnet.private_subnets.*.id
}

output "public_subnet_cidrs" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public_subnets.*.cidr_block
}

output "public_subnet_ids" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public_subnets.*.id
}

output "flow_log_role_name" {
  description = "Plain text name of the IAM role used for VPC flow logs."
  value       = aws_iam_role.vpc_flow_log_role.name
}

output "flow_log_policy_arn" {
  description = "Amazon Resource Name of the IAM role used for VPC flow logs."
  value       = aws_iam_policy.vpc_flow_log_policy.arn
}
