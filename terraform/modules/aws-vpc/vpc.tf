data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "main" {

  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  enable_dns_hostnames             = true
  enable_dns_support               = true
  enable_classiclink               = false
  enable_classiclink_dns_support   = false
  assign_generated_ipv6_cidr_block = false

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-vpc"
    )
  )
}

resource "aws_internet_gateway" "main" {

  vpc_id = aws_vpc.main.id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-igw"
    )
  )
}

resource "aws_default_network_acl" "default" {

  default_network_acl_id = aws_vpc.main.default_network_acl_id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-default"
    )
  )
}

resource "aws_default_route_table" "default" {

  default_route_table_id = aws_vpc.main.default_route_table_id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-default"
    )
  )
}

resource "aws_default_security_group" "default" {

  vpc_id = aws_vpc.main.id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-default"
    )
  )
}

resource "aws_subnet" "private_subnets" {

  count = length(var.private_subnet_cidr)

  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(var.private_subnet_cidr, count.index)
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = false

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-private-0${count.index + 1}"
    )
  )
}

resource "aws_subnet" "public_subnets" {

  count = length(var.public_subnet_cidr)

  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(var.public_subnet_cidr, count.index)
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = true

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-public-0${count.index + 1}"
    )
  )
}
