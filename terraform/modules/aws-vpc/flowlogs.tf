data "aws_iam_policy_document" "vpc_flow_log_document" {

  statement {

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "vpc_flow_log_role" {
  name               = "${var.project}-${var.environment}-vpc-flow-logs"
  description        = "IAM role attached to ${var.environment} VPC to allow flow log creation of within CloudWatch logs"
  assume_role_policy = data.aws_iam_policy_document.vpc_flow_log_document.json
  path               = "/"
}

resource "aws_iam_policy" "vpc_flow_log_policy" {
  name        = "${var.project}-${var.environment}-vpc-flow-logs"
  description = "IAM policy allowing log creation from a LVPC flow logs"
  path        = "/"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [{
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams"
            ],
            "Resource": "arn:aws:logs:*:*:*",
            "Effect": "Allow"
        }
    ]
}
POLICY
}

resource "aws_cloudwatch_log_group" "vpc_flow_log_group" {

  name = "/aws/vpc/${var.project}-${var.environment}-flow-logs"

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-flow-logs"
    )
  )
}

resource "aws_flow_log" "vpc_flow_logs" {
  iam_role_arn    = aws_iam_role.vpc_flow_log_role.arn
  log_destination = aws_cloudwatch_log_group.vpc_flow_log_group.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.main.id
}
