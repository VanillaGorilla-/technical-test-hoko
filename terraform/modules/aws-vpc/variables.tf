variable "application" {
  type        = string
  description = "Application name."
}

variable "environment" {
  type        = string
  description = "Environment name, used for stack separation."
}

variable "project" {
  type        = string
  description = "Project this resource belongs to."
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR block for the entire VPC"
}

variable "public_subnet_cidr" {
  type        = list
  description = "CIDR range of the public subnet."
  default     = []
}

variable "private_subnet_cidr" {
  type        = list
  description = "CIDR range of the private subnet."
  default     = []
}
