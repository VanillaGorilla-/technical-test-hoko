resource "aws_eip" "nat" {

  depends_on = [aws_internet_gateway.main]

  vpc = true

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-nat"
    )
  )
}

resource "aws_nat_gateway" "nat_gw" {

  depends_on = [aws_internet_gateway.main]

  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_subnets[0].id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-nat"
    )
  )
}
