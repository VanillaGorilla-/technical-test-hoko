resource "aws_route_table" "public_route_table" {

  count = length(var.public_subnet_cidr)

  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-public-route-table-0${count.index + 1}"
    )
  )
}

resource "aws_route_table_association" "public" {

  count = length(var.public_subnet_cidr)

  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.public_route_table.*.id, count.index)
}

resource "aws_route_table" "private_route_table" {

  count = length(var.private_subnet_cidr)

  vpc_id = aws_vpc.main.id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-private-route-table-0${count.index + 1}"
    )
  )
}

resource "aws_route_table_association" "private" {

  count = length(var.private_subnet_cidr)

  subnet_id      = element(aws_subnet.private_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.private_route_table.*.id, count.index)
}

resource "aws_route" "nat" {

  count = length(var.private_subnet_cidr)

  destination_cidr_block = "0.0.0.0/0"
  route_table_id         = element(aws_route_table.private_route_table.*.id, count.index)
  nat_gateway_id         = aws_nat_gateway.nat_gw.id
}
