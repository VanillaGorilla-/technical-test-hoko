resource "aws_network_acl" "public" {

  vpc_id     = aws_vpc.main.id
  subnet_ids = aws_subnet.public_subnets.*.id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-public-nacl"
    )
  )
}

resource "aws_network_acl_rule" "public_egress" {

  network_acl_id = aws_network_acl.public.id
  egress         = true
  rule_number    = 100
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl_rule" "public_ingress" {

  network_acl_id = aws_network_acl.public.id
  egress         = false
  rule_number    = 100
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl" "private" {

  vpc_id     = aws_vpc.main.id
  subnet_ids = aws_subnet.private_subnets.*.id

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-private-nacl"
    )
  )
}

resource "aws_network_acl_rule" "private_egress" {

  network_acl_id = aws_network_acl.private.id
  egress         = true
  rule_number    = 100
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl_rule" "private_ingress" {

  network_acl_id = aws_network_acl.private.id
  egress         = false
  rule_number    = 100
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}
