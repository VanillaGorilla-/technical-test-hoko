resource "aws_iam_role" "ecs_task_execution_role" {

  name_prefix = "${var.project}-${var.environment}-ecs-task-execution"

  assume_role_policy = data.aws_iam_policy_document.ecs_task.json
  path               = "/"
}

resource "aws_iam_role" "ecs_task_role" {

  name_prefix = "${var.project}-${var.environment}-ecs-task"

  assume_role_policy = data.aws_iam_policy_document.ecs_task.json
  path               = "/"
}

data "aws_iam_policy_document" "ecs_task" {
  statement {

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_policy" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_task_policy" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = var.ecs_task_role_policy
}
