resource "aws_ecs_cluster" "cluster" {

  name = "${var.project}-${var.environment}-ecs-cluster"

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-ecs-cluster"
    )
  )
}

resource "aws_ecs_task_definition" "fargate" {
  family                   = "${var.project}-${var.environment}-ecs-task-definition"
  container_definitions    = data.template_file.qledger.rendered
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory
  requires_compatibilities = ["FARGATE"]

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-ecs-task-definition"
    )
  )
}
