variable "application" {
  type        = string
  description = "Application name."
}

variable "cluster_endpoint" {
  type        = string
  description = "The DNS address of the RDS instance"
}

variable "cpu" {
  type        = string
  description = "The number of cpu units used by the task."
}

variable "memory" {
  type        = string
  description = "The amount (in MiB) of memory used by the task."
}

variable "ecs_task_role_policy" {
  type        = string
  description = "The ARN of IAM role that allows ECS container task to make calls to other AWS services."
}

variable "environment" {
  type        = string
  description = "Environment name, used for stack separation."
}

variable "password" {
  type        = string
  description = "Password to use when connecting to the database."
}

variable "project" {
  type        = string
  description = "Project this resource belongs to."
}

variable "username" {
  type        = string
  description = "Username to use when connecting to the database."
}
