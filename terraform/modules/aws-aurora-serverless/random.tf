# Generates a random random values that will be used with the database.
# Passed to other AWS resources with Terraform.

resource "random_password" "master_password" {
  length  = 10
  special = false # Whether to include special characters in the password (i.e. !@#$%&*()-_=+[]{}<>:?).
}

resource "random_password" "master_username" {
  length  = 10
  special = false # Whether to include special characters in the password (i.e. !@#$%&*()-_=+[]{}<>:?).
}
