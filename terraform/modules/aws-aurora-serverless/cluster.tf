resource "aws_rds_cluster" "aurora" {
  database_name          = var.database_name
  db_subnet_group_name   = aws_db_subnet_group.aurora.name
  enable_http_endpoint   = true
  engine                 = var.engine
  engine_mode            = var.engine_mode
  engine_version         = var.engine_version
  master_password        = random_password.master_password.result
  master_username        = random_password.master_username.result
  port                   = var.port
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.aurora.id]
  scaling_configuration {
    auto_pause               = var.scaling_auto_pause
    max_capacity             = var.scaling_max_capacity
    min_capacity             = var.scaling_min_capacity
    seconds_until_auto_pause = var.scaling_seconds_until_auto_pause
    timeout_action           = var.scaling_timeout_action
  }

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-aurora-cluster"
    )
  )
}

resource "aws_db_subnet_group" "aurora" {
  name_prefix = "${var.project}-${var.environment}-db-subnet-group-"
  description = "For Aurora cluster ${var.project} ${var.environment} Aurora cluster"
  subnet_ids  = var.db_subnets

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-db-sudbnet-group"
    )
  )
}
