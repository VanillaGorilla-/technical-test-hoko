variable "application" {
  type        = string
  description = "Application name."
}

variable "database_name" {
  type        = string
  description = "Logical name of the database."
}

variable "db_subnets" {
  type        = list
  description = "Subnets to include in the DB subnet group."
}

variable "engine" {
  type        = string
  description = "The name of the database engine to be used for the database."
}

variable "engine_mode" {
  type        = string
  description = "The database engine mode."
}

variable "engine_version" {
  type        = string
  description = "The database engine version."
}

variable "environment" {
  type        = string
  description = "Environment name, used for stack separation."
}

variable "port" {
  type        = string
  description = "The port which will be used for establishing connections to the database."
}

variable "project" {
  type        = string
  description = "Project this resource belongs to."
}

variable "scaling_auto_pause" {
  type        = bool
  description = "Whether to enable automatic pause."
}

variable "scaling_max_capacity" {
  type        = number
  description = " The maximum scaling capacity."
}

variable "scaling_min_capacity" {
  type        = number
  description = " The minimum scaling capacity."
}

variable "scaling_seconds_until_auto_pause" {
  type        = number
  description = "The time, in seconds, before an Aurora DB cluster in serverless mode is paused."
}

variable "scaling_timeout_action" {
  type        = string
  description = "The action to take when the timeout is reached."
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR block of the VPC; used with security groups."
}

variable "vpc_id" {
  type        = string
  description = "Logical ID of the VPC; used with security groups."
}
