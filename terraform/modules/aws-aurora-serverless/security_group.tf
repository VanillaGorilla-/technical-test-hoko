resource "aws_security_group" "aurora" {

  name_prefix = "${var.project}-${var.environment}-aurora-"
  vpc_id      = var.vpc_id

  description = "Control traffic to & from ${var.project} ${var.environment} Aurora cluster."

  tags = merge(
    local.standard_tags,
    map(
      "Name", "${var.project}-${var.environment}-aurora"
    )
  )
}

resource "aws_security_group_rule" "cidr_block_ingress" {

  description       = "Allowing inbound traffic from permitted CIDR blocks"
  type              = "ingress"
  from_port         = aws_rds_cluster.aurora.port
  to_port           = aws_rds_cluster.aurora.port
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.aurora.id
}

resource "aws_security_group_rule" "security_group_self" {

  description       = "Allowing all traffic originating from within the security group."
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  self              = true
  security_group_id = aws_security_group.aurora.id
}
