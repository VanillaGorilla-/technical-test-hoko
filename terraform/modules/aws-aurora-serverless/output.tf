output "aurora_port" {
  description = "Port number used for connecting to the database."
  value       = aws_rds_cluster.aurora.port
}

output "aurora_security_group_id" {
  description = "Logical ID of the Aurora cluster security group."
  value       = aws_security_group.aurora.id
}

output "cluster_endpoint" {
  description = "The DNS address of the RDS instance"
  value       = aws_rds_cluster.aurora.endpoint
}

output "cluster_master_password" {
  description = "The databse master password"
  value       = aws_rds_cluster.aurora.master_password
  sensitive   = true # Prevents the password from being shown after 'terraform apply'.
  # Should only be accessed by other Terraform resources. Should never be used by a human.
}

output "cluster_master_username" {
  description = "The databse master username"
  value       = aws_rds_cluster.aurora.master_username
  sensitive   = true # Prevents the username from being shown after 'terraform apply'.
  # Should only be accessed by other Terraform resources. Should never be used by a human.
}

output "cluster_port" {
  description = "The port of the RDS instance"
  value       = aws_rds_cluster.aurora.port
}
