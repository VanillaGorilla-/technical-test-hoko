module "vpc" {
  source = "./modules/aws-vpc"

  application         = var.application
  environment         = var.environment
  private_subnet_cidr = var.private_subnet_cidr
  project             = var.project
  public_subnet_cidr  = var.public_subnet_cidr
  vpc_cidr            = var.vpc_cidr
}
