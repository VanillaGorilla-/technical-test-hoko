module "fargate" {
  source = "./modules/aws-ecs-fargate"

  # This dependancy is to allow the DB to be deployed first
  # so the application can successsfully connect once deployed.
  depends_on = [module.aurora]

  application          = var.application
  cluster_endpoint     = local.connection_string
  cpu                  = 256
  ecs_task_role_policy = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  environment          = var.environment
  memory               = 512
  password             = module.aurora.cluster_master_password
  project              = var.project
  username             = module.aurora.cluster_master_username
}

# Using a 'locals' block to concatinate the full string to connect to the Aurora database.
locals {
  connection_string = join(
    "",
    [
      "postgresql://", module.aurora.cluster_master_username,
      ":",
      module.aurora.cluster_master_password,
      "@", module.aurora.cluster_endpoint,
      "/qledger?sslmode=disable",
      ":",
      module.aurora.cluster_port
    ]
  )
}
