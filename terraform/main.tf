# A standard set of tags that will be attached to every possible resource created with Terraform.
locals {
  standard_tags = "${
    map(
      "Application", var.application,
      "Environment", var.environment,
      "Project", var.project,
      "Managed", "Terraform",
      "Team", var.team
    )
  }"
}
