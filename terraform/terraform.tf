# Used to configure some behaviors of Terraform itself.
# https://www.terraform.io/docs/configuration/terraform.html

terraform {
  # Setting the required version of Terraform itself.
  # In this case, constrained to v0.13.x
  required_version = "~> 0.13"
}
