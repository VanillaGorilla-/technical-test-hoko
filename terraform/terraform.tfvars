#################
# STANDARD TAGS #
#################
application = "QLedger"
environment = "dev"
project     = "take-home"
team        = "Onucki"

#################
# VPC VARIABLES #
#################
private_subnet_cidr = [
  "10.0.4.0/24",
  "10.0.5.0/24",
  "10.0.6.0/24",
]
public_subnet_cidr = [
  "10.0.1.0/24",
  "10.0.2.0/24",
  "10.0.3.0/24",
]
vpc_cidr = "10.0.0.0/16"
