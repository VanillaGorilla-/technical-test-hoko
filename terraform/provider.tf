# https://www.terraform.io/docs/providers/index.html

# Used for interacting with AWS resources.
provider "aws" {
  region = "us-east-1"
}
