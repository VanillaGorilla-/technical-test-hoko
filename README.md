# Technical Test - Ben Onucki

Greetings!

Welcome to my submission for the technical test.

After reviewing the instructions, requirements and bonus points I came up with my approach; go serverless wherever possible. By going serverless, we eliminatet the need to provision and manage traditional infrastructure and security controls.

With that in mind I chose to use [Amazon Aurora Serverless](https://aws.amazon.com/rds/aurora/serverless/) for the Postgresql database, and [AWS Fargate](https://aws.amazon.com/fargate/) to host the QLedger docker image. Both of these solutions are a managed solutions, which means Amazon takes care of all the underlying resources, and "we" just need to manage the application(s).

I wrote all the Infrastructure-As-Code using Terraform 0.13.x, so I can add the `depends_on` flag to the modules themselves so as to not create race conditions and eliminate dependency issues for unprovisioned resources.

Please continue reading below to see how to deploy the code.

## Architecture Overview

![Architecture](images/Architecture.png)

## Installation

All work was performed on a Linux-based machine.

### Assumptions

- This document is under the assumption you have a working knowledge of AWS and Terraform.
- The user executing this code has the proper permissions and authority to do so against the respective AWS environment.

### Prerequisites

- [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- [Terraform CLI v 0.13.x](https://learn.hashicorp.com/tutorials/terraform/install-cli)

### Deployment Steps

1. Follow the installation steps for both prerequiites.

2. Configure the AWS CLI to access your desired AWS environment, using the method you're most comfortable with (RBAC, access keys, etc.).

3. Clone or download the repository.

4. From within the repo, navigate to the `terraform/` directory.

5. Verify the values in `terraform.tfvars` won't conflict with any existing resources in your AWS account.

6. Do `terraform plan`, review output, address any errors, inconsistencies and/or issues.

7. Once satisfied do `terraform apply`, review output again, type `yes` or `no` to deploy these resources.

## Usage

The Terraform code will automatically connect QLedger to the database via `task-definition/` in the Fargate module, so no post-configuration needed.

## Author

Ben Onucki - jbonucki@gmail.com
